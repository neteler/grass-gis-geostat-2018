Legend from

 Kottek, M., J. Grieser, C. Beck, B. Rudolf, and F. Rubel, 2006: World Map of
   the Köppen-Geiger climate classification updated. Meteorol. Z., 15, 259-263.
   DOI: 10.1127/0941-2948/2006/0130

   http://koeppen-geiger.vu-wien.ac.at/present.htm

   http://koeppen-geiger.vu-wien.ac.at/pdf/Paper_2006.pdf

