# The trainer

Markus Neteler is partner and general manager at [mundialis](https://www.mundialis.de) GmbH & Co. KG, Bonn, Germany. From 2001-2015 he worked as a researcher in Italy. Markus is co-founder of OSGeo and since 1998, coordinator of the GRASS GIS development (for details, see his private [homepage](https://grassbook.org/neteler/)). 

# Any feedback?

Please write to me at: neteler @ osgeo.org

- Repository of this material on [gitlab](https://gitlab.com/neteler/grass-gis-geostat-2018/tree/master/intro)
- [Privacy](https://about.gitlab.com/privacy/)
